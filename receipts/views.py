from django.shortcuts import render
from receipts.models import Receipt


def show_receipt(request):
    all_receipts = Receipt.objects.all()
    context = {
        "receipts": all_receipts,
    }
    return render(request, "receipts/receipts.html", context)
